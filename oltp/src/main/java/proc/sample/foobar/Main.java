package proc.sample.foobar;

import akka.actor.ActorRef;
import proc.core.channel.MessageChannel;
import proc.core.channel.MessageChannelFactory;
import proc.core.channel.RabbitMqChannel;
import proc.core.message.ProcMessage;
import proc.core.message.ProcMessageImpl;
import proc.core.service.ActorManager;
import proc.core.service.TaskManager;
import proc.sample.foobar.actor.Bar;
import proc.sample.foobar.actor.Foo;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by johnyr on 1/07/15.
 */
public class Main {
    public static void main(String[] args) {
        Main.fillTaskQueue();
        Main.initProc();
    }

    protected static void fillTaskQueue()
    {
        RabbitMqChannel<ProcMessage> channel = new RabbitMqChannel<ProcMessage>("processing", "localhost");
        for (int i = 0; i <= 10; i++) {
            ProcMessageImpl message = new ProcMessageImpl("foo", "bar", "test");
            channel.push(message);
        }
    }

    protected static void initProc()
    {
        MessageChannel channel = MessageChannelFactory.createRabbitMQ();

        Map<String, Class> actorMap = new HashMap<>();
        actorMap.put("foo", Foo.class);
        actorMap.put("bar", Bar.class);

        ActorRef taskActor = TaskManager.create(channel, ActorManager.createActorCatalog(actorMap, channel));

        taskActor.tell("run", taskActor);
    }
}
