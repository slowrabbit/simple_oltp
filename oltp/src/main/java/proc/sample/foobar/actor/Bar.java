package proc.sample.foobar.actor;

import proc.core.actor.BaseActor;
import proc.core.channel.MessageChannel;

/**
 * Created by johnyr on 1/07/15.
 */
public class Bar extends BaseActor {
    public Bar(String actorName, MessageChannel channel) {
        super(actorName, channel);
    }

    @Override
    public void preStart(){
        System.out.println("-------");
    }

    @Override
    public void execute(Object msg) {
        System.out.println("I`m BAR!");
        System.out.println("I receive " + msg.toString());
        sendMessage("foo", "bartest");
    }
}
