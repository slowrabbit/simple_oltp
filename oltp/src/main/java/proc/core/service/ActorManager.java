package proc.core.service;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import proc.core.channel.MessageChannel;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by johnyr on 1/07/15.
 */
public class ActorManager {
    @SuppressWarnings("unchecked")
    public static Map<String, ActorRef> createActorCatalog(Map<String, Class> actorCatalogMap, MessageChannel channel) {
        Map actorCatalog = new HashMap<String, ActorRef>();
        for (Map.Entry<String, Class> entry : actorCatalogMap.entrySet()) {
            actorCatalog.put(entry.getKey(), ActorSystem.create().actorOf(Props.create(entry.getValue(), entry.getKey(), channel)));
        }

        return actorCatalog;
    }
}
