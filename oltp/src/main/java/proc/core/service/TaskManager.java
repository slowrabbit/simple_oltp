package proc.core.service;

import akka.actor.*;
import proc.core.channel.MessageChannel;
import proc.core.message.ProcMessage;

import java.util.Map;

/**
 * Created by johnyr on 1/07/15.
 */
public class TaskManager extends UntypedActor {

    MessageChannel channel;
    Map<String, ActorRef> actorCatalog;

    public TaskManager(MessageChannel channel, Map<String, ActorRef> actorCatalog) {
        this.channel = channel;
        this.actorCatalog = actorCatalog;
    }

    protected void assignTasks() {
        while (true) {
            try {
                ProcMessage message = (ProcMessage) channel.pop();
                ActorRef actor = actorCatalog.get(message.getRecipient());
                actor.tell(message, getSelf());
            } catch (NullPointerException e) {
                continue;
            }

        }
    }

    @Override
    public void onReceive(Object message) {
        if (message.equals("run")) {
            try {
                assignTasks();
            } catch (Throwable cause) {
                throw new RuntimeException(cause);
            }
        } else {
            unhandled(message);
        }
    }

    public static ActorRef create(MessageChannel channel, Map<String, ActorRef> actorCatalog) {
        return ActorSystem.create().actorOf(Props.create(TaskManager.class, channel, actorCatalog));
    }
}
