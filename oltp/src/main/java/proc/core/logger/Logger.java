package proc.core.logger;

/**
 * Created by johnyr on 30/06/15.
 */
public interface Logger {
    default void log(String msg) {
        System.out.println("Log:" + msg);
    }
}
