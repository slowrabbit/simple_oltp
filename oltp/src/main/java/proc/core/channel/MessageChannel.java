package proc.core.channel;

/**
 * Created by johnyr on 30/06/15.
 */

public interface MessageChannel<T> {
    String getName();

    void push(T message);

    T pop();
}
