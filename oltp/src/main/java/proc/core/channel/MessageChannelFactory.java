package proc.core.channel;

import proc.core.message.ProcMessage;

/**
 * Created by johnyr on 1/07/15.
 */
public class MessageChannelFactory {
    public static MessageChannel createRabbitMQ() {
        return new RabbitMqChannel<ProcMessage>("processing", "localhost");
    }
}
