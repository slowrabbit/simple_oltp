package proc.core.channel;

import com.rabbitmq.client.*;
import org.apache.commons.lang.SerializationUtils;
import proc.core.logger.Logger;
import java.io.Serializable;


/**
 * Created by johnyr on 30/06/15.
 */
public class RabbitMqChannel<T> implements MessageChannel<T> {
    protected String name;
    protected String host;
    protected Logger logger;

    public RabbitMqChannel(String name, String host) {
        this.name = name;
        this.host = host;
        this.logger = new Logger() {
            @Override
            public void log(String msg) {
                System.out.println("Log:" + msg);
            }
        };
    }

    public RabbitMqChannel(String name, String host, Logger logger) {
        this.name = name;
        this.host = host;
        this.logger = logger;
    }

    public String getName() {
        return name;
    }

    public String getHost() {
        return host;
    }

    public void push(T message) {
        Channel channel = this.createChannel(this.host);
        try {
            channel.queueDeclare(name, false, false, false, null);
            channel.basicPublish("", name, null, SerializationUtils.serialize((Serializable) message));

            logger.log(" [x] Sent '" + message.toString() + "'");

            channel.close();
            channel.getConnection().close();
        } catch (Exception e) {
            logger.log(e.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    public T pop() {
        T message = null;
        try {
            Channel channel = this.createChannel(this.host);

            logger.log(" [*] Waiting for messages");

            GetResponse response = channel.basicGet(name, true);

            message = (T) SerializationUtils.deserialize(response.getBody());

            logger.log(" [x] Received '" + message + "'");

            channel.close();
            channel.getConnection().close();
        } catch (Exception e) {
            logger.log(e.getMessage());
        }

        return message;
    }

    protected Channel createChannel(String host) {
        Channel channel = null;
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(host);
            Connection connection = factory.newConnection();
            channel = connection.createChannel();
        } catch (Exception e) {
            logger.log(e.getMessage());
        }

        return channel;
    }
}
