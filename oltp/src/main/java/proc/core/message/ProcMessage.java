package proc.core.message;

import java.io.Serializable;

/**
 * Created by johnyr on 29/06/15.
 */
public interface ProcMessage extends Serializable {
    String getSender();
    String getRecipient();
    Object getData();
}
