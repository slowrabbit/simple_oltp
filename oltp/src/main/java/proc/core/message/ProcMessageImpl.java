package proc.core.message;

/**
 * Created by johnyr on 1/07/15.
 */
public class ProcMessageImpl implements ProcMessage {
    String sender;
    String recipient;
    Object data;

    public ProcMessageImpl(String sender, String recipient, Object data) {
        this.sender = sender;
        this.recipient = recipient;
        this.data = data;
    }

    @Override
    public String getSender() {
        return sender;
    }

    @Override
    public String getRecipient() {
        return recipient;
    }

    @Override
    public Object getData() {
        return data;
    }
}
