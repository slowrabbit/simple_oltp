package proc.core.actor;

import akka.actor.UntypedActor;
import proc.core.channel.MessageChannel;
import proc.core.logger.Logger;
import proc.core.message.ProcMessageImpl;

/**
 * Created by johnyr on 29/06/15.
 */
public abstract class BaseActor extends UntypedActor {

    Logger logger;
    String actorName;
    MessageChannel channel;

    public BaseActor(String actorName, MessageChannel channel) {
        this.actorName = actorName;
        this.channel = channel;
        this.logger = new Logger() {
            @Override
            public void log(String msg) {
                System.out.println("Log:" + msg);
            }
        };
    }

    protected void init(String actorName, MessageChannel channel) {
        this.actorName = actorName;
        this.channel = channel;
        this.logger = new Logger() {
            @Override
            public void log(String msg) {
                System.out.println("Log:" + msg);
            }
        };
    }

    public String getActorName() {
        return actorName;
    }

    @SuppressWarnings("unchecked")
    public void sendMessage(String recipientName, Object data) {
        ProcMessageImpl message = new ProcMessageImpl(this.getActorName(), recipientName, data);
        channel.push(message);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onReceive(Object msg) {
        try {
            execute(msg);
        } catch (Throwable cause) {
            throw new RuntimeException(cause);
        }
    }

    abstract public void execute(Object msg);
}